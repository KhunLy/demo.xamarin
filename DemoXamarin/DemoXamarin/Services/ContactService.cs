﻿using DemoXamarin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DemoXamarin.Services
{
    class ContactService
    {
        public IEnumerable<Contact> Get()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage message
                    = client.GetAsync("http://10.0.2.2:3000/contacts").Result;
                if(message.IsSuccessStatusCode)
                {
                    string json = message.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<Contact>>(json);
                }
                throw new HttpRequestException();
            }
        }

        public bool Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage message 
                    = client.DeleteAsync("http://10.0.2.2:3000/contacts/" + id).Result;
                if(message.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        public bool Update(ContactForm c, int id)
        {
            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(c);
                StringContent content 
                    = new StringContent(json, Encoding.UTF8, 
                    "application/json");
                HttpResponseMessage message =
                client.PutAsync("http://10.0.2.2:3000/contacts/" + id
                    , content).Result;
                return message.IsSuccessStatusCode;
            }
        }

        public bool Create(ContactForm c)
        {
            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(c);
                StringContent content
                    = new StringContent(json, Encoding.UTF8,
                    "application/json");
                HttpResponseMessage message =
                client.PostAsync("http://10.0.2.2:3000/contacts"
                    , content).Result;
                return message.IsSuccessStatusCode;
            }
        }
    }
}
