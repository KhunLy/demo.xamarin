﻿using DemoXamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoXamarin.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactDetailsModal : ContentPage
	{
		public ContactDetailsModal (ContactDetailsModalVM vm = null)
		{
			BindingContext = vm ?? new ContactDetailsModalVM();
			InitializeComponent ();
		}
	}
}