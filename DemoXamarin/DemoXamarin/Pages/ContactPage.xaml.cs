﻿using DemoXamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoXamarin.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactPage : ContentPage
    {
        public ContactPage()
        {
            //BindingContext = new ContactPageVM();
            InitializeComponent();
        }

        public void OpenAddModal(object sender, EventArgs args)
        {
            Application.Current.MainPage.Navigation
                .PushModalAsync(new ContactDetailsModal());
        }
    }
}