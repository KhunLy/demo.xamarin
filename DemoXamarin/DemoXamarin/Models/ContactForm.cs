﻿using DemoXamarin.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoXamarin.Models
{
    public class ContactForm
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
    }
}
