﻿using DemoXamarin.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using DemoXamarin.Models;
using DemoXamarin.Services;

namespace DemoXamarin.ViewModels
{
    public class ContactDetailsModalVM: BindableObject
    {
        public int? Id { get; set; }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; OnPropertyChanged(); }
        }

        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; OnPropertyChanged(); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged(); }
        }

        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; OnPropertyChanged(); }
        }

        private Gender gender;

        public Gender Gender
        {
            get { return gender; }
            set { gender = value; OnPropertyChanged(); }
        }

        private DateTime birthDate;

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; OnPropertyChanged(); }
        }

        public List<Gender> Genders
        {
            get 
            {
                return Enum.GetValues(typeof(Gender))
                    .Cast<Gender>()
                    .ToList();
            }
        }

        public Command SaveCommand { get; set; }

        public ContactDetailsModalVM()
        {
            SaveCommand = new Command(Save);
        }

        public void Save()
        {
            if(Id == null)
            {
                // post
                DependencyService.Get<ContactService>()
                    .Create(new ContactForm { 
                        LastName = LastName,
                        FirstName = FirstName,
                        BirthDate = BirthDate,
                        Email = Email,
                        Gender = Gender,
                        Phone = Phone
                    });
            }
            else
            {
                // put
                DependencyService.Get<ContactService>()
                    .Update(new ContactForm
                    {
                        LastName = LastName,
                        FirstName = FirstName,
                        BirthDate = BirthDate,
                        Email = Email,
                        Gender = Gender,
                        Phone = Phone
                    }, Id.Value);
            }

            Application.Current.MainPage.Navigation.PopModalAsync();

            MessagingCenter.Send(this, "CONTACT_UPDATED");
        }

    }
}
