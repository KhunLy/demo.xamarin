﻿using DemoXamarin.Enums;
using DemoXamarin.Models;
using DemoXamarin.Pages;
using DemoXamarin.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DemoXamarin.ViewModels
{


    class ContactPageVM : BindableObject
    {

        private ObservableCollection<Contact> contacts;
        public ObservableCollection<Contact> Contacts
        {
            get { return contacts; }
            set
            {
                if (contacts != value)
                {
                    contacts = value;
                    // A chaque changements de la propriété
                    // prévenir la vue
                    OnPropertyChanged();
                }
            }
        }

        public Command DeleteCommand { get; set; }
        public Command DetailsCommand { get; set; }

        public ContactPageVM()
        {
            MessagingCenter.Subscribe<ContactDetailsModalVM>(this, "CONTACT_UPDATED", 
                contact => 
                {
                    Contacts = new ObservableCollection<Contact>(
                        DependencyService.Get<ContactService>().Get()
                    );
                }
            );
            // chargement sur une api ou une db
            Contacts = new ObservableCollection<Contact>(
                DependencyService.Get<ContactService>().Get()
            );
            

            DeleteCommand = new Command<Contact>(Delete);
            DetailsCommand = new Command<Contact>(Details);
        }

        private void Details(Contact c)
        {
            Application.Current.MainPage.Navigation.PushModalAsync(
                new ContactDetailsModal(new ContactDetailsModalVM { 
                    Id = c.Id,
                    LastName = c.LastName,
                    FirstName = c.FirstName,
                    Email = c.Email,
                    BirthDate = c.BirthDate,
                    Gender = c.Gender,
                    Phone = c.Phone
                })
            );
        }

        private async void Delete(Contact c) 
        {
            bool result =
                await Application.Current.MainPage.DisplayAlert("Confimartion", "Etes ... ?", "oui", "non");
            if(result)
            {
                if(DependencyService.Get<ContactService>().Delete(c.Id))
                {
                    Contacts.Remove(c);
                    Application.Current.MainPage.DisplayAlert("Suppression OK", "", "ok");
                }
            }
        }
    }
}
